FROM --platform=linux/arm64 node:lts-alpine as build

COPY . .
RUN npm ci
RUN npm run build

FROM node:lts-alpine as run

WORKDIR /server
COPY --from=build /dist/ ./dist/
COPY node_modules .
RUN mkdir log

CMD ["node", "dist/index.js"]
